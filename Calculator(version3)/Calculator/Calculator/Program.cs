﻿using Calculator.ManagerFolder;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CalcBL;

namespace Calculator
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Int or Float?");
            String type = Convert.ToString(Console.ReadLine());
            Console.Write("a = ");
            string first = Console.ReadLine();
            Console.Write("operation = ");
            string operation = Console.ReadLine();
            Console.Write("b = ");
            string second = Console.ReadLine();
            switch (type)
            {
                case "Int":
                    try {
                        Console.WriteLine(Manager.OutputData(new IntCalc { First = Convert.ToInt32(first), Operation = operation, Second = Convert.ToInt32(second) }));
                        }
                    catch (System.FormatException ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                    break;
                    
                case "Float":
                   try
                    {
                        Console.WriteLine(Manager.OutputData(new FloatCalc { First = Convert.ToSingle(first), Operation = operation, Second = Convert.ToSingle(second) }));
                    }
                    catch (System.FormatException ex)
                    {
                        Console.WriteLine(ex.Message);
                    }
                    break;
            }
            Console.ReadKey();
        }
    }
}
