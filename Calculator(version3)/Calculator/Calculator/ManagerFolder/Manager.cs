﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CalcBL;

namespace Calculator.ManagerFolder
{
    class Manager
    {
        public static string OutputData(CalcAbstract calcAbstract)
        {
            return (calcAbstract.DoCalc());
        }
    }
}
