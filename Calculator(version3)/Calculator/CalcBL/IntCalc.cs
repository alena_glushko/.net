﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalcBL
{
    public class IntCalc:CalcAbstract
    {
        public string Operation { get; set; }
        public int First { get; set; }
        public int Second { get; set; }

        public override string DoCalc()
        {
            if (Operation == "+") return (Convert.ToString(First + Second));
            if (Operation == "-") return (Convert.ToString(First - Second));
            if (Operation == "*") return (Convert.ToString(First * Second));
            if (Operation == "/")
            {
                try
                {
                    return (Convert.ToString(First / Second));
                }
                catch (System.DivideByZeroException e)
                {
                    return (e.Message);
                }
            }            
           else return "Unknown operation!";
        }
    }
}
