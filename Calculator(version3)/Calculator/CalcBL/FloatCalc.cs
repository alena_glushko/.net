﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalcBL
{
   public class FloatCalc:CalcAbstract
    {
        public string Operation { get; set; }
        public float First { get; set; }
        public float Second { get; set; }

        public override string DoCalc()
        {
            if (Operation == "+") return Convert.ToString(First + Second);
            if (Operation == "-") return Convert.ToString(First - Second);
            if (Operation == "*") return Convert.ToString(First * Second);
            if (Operation == "/")
            {
                if (float.IsInfinity(First / Second) ==true) return "Infinity!";
                else return Convert.ToString(First / Second);
            }
            else return "Unknown operation!";
        }
    }
}
