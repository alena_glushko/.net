﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CalcBL
{
    public abstract class CalcAbstract
    {
        public abstract string DoCalc();
    }
}
